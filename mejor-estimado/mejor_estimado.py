# -*- coding: utf-8 -*-

import numpy as np

datos1=[11.3, 11.2, 11.1, 11.3, 11.2, 11.2]
datos2=[1.5, 1.7, 1.6, 1.5, 1.5, 1.6]
datos3=[5.5, 5.4, 5.4, 5.4, 5.3, 5.4]
sB=0.1
decimales=1

Ndatos=len(datos1)

print ("datos cargados")
print("número de datos: ",Ndatos)

"""**Cálculo del promedio**

"""

## Cálculo promedio para medidas repetidas##
def promedio(data):
    prom=np.mean(data)
    print("\n \n promedio: ",prom)
    print("redondeo a ",decimales, " decimales: ", np.round(prom,decimales), " cm")
    return prom

"""**Cálculo de la desviación estandar muestral ó incertidumbre típica**


"""

#codificar el cálculo de la incertidumbre típica
def itipica(data, Ndatos, prom):
    i=0
    suma=0
    while i < Ndatos:
        suma=suma+(data[i]-prom)**2 #Usamos el prom pasado como parámetro
        i+=1

    sx=np.sqrt(suma/(Ndatos-1))  

    print("\n desviación estándar (incert. típica)", sx) 
    print("redondeo a ",decimales, " decimales: ",  np.round(sx,decimales), " cm")
    return sx

"""**Cálculo de la incertidumbre tipo A**
"""

#codificar el cálculo de la incertidumbre tipo A
def itipoA(sx):
    sA=sx/np.sqrt(Ndatos)

    print("\n incert. tipo A:",sA)
    print("redondeo a ",decimales, " decimales: ",np.round(sA,decimales), " cm")
    return sA

"""**Cálculo de la incertidumbre combinada**

"""

#codificar el cálculo de la incertidumbre combinada
def ucombinada(sA,sB):
    uc=np.sqrt(sA**2+sB**2)

    print("\n incert. combinada:",uc)
    print("redondeo a ",decimales, " decimales: ",np.round(uc,decimales), " cm")

    return uc


print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")

def foo(datos):
    prom=promedio(datos)
    sx=itipica(datos, Ndatos, prom)
    sA=itipoA(sx)
    uc=ucombinada (sA,sB)
    print("\n\n  Mejor estimado: (",np.round(prom,decimales), "+/-", np.round(uc,decimales),") cm")
    print("\n______________________________________________________________________")


foo(datos1)
foo(datos2)
foo(datos3)
